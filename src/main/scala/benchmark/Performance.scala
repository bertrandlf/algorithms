package benchmark

/**
  * Created by Bertrand on 2/10/2016.
  */
object Performance {

  def timeIt[A](f: => A) = {
    val start = System.nanoTime()
    val result = f
    (s"Took ${(System.nanoTime() - start)/ 1000000.0}ms", result)
  }

}
