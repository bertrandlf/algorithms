package week1

import week1.model.ListInversions

import scala.collection.mutable.ListBuffer

/**
  * Created by Bertrand on 10/09/2016.
  */
object CountInversions {

  val emptyInversions = new ListInversions(Nil, 0)

  def countInversions(list: List[Int]): ListInversions = list match {
    case h :: Nil => new ListInversions(list, 0)
    case h :: t =>
      if (t.length == 1) {
        if (h <= t.head) {
          new ListInversions(list, 0)
        }
        else {
          new ListInversions(List(t.head, h), 1)
        }
      } else {
        val split = list.splitAt(list.length / 2)
        mergeSortAndCountInversions(countInversions(split._1), countInversions(split._2))
      }
    case _ => emptyInversions
  }

  def mergeSortAndCountInversions(al: ListInversions, bl: ListInversions): ListInversions = {
    val a = al.list
    val b = bl.list
    val result = ListBuffer[Int]()
    var i, j = 0
    var splitInversions = 0
    while (j < b.length || i < a.length) {
      if (j == b.length) {
        //b's end reached
        result += a(i)
        i += 1
      } else if (i == a.length) {
        //a's end reached
        result += b(j)
        j += 1
      } else {
        if (a(i) <= b(j)) {
          result += a(i)
          i += 1
        } else {
          result += b(j)
          j += 1
          splitInversions += a.length - i
        }
      }
    }
    new ListInversions(result.toList, al.inversions + bl.inversions + splitInversions)
  }

}
