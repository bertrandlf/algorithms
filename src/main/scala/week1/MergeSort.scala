package week1

import scala.collection.mutable.ListBuffer

/**
  * Created by Bertrand on 10/09/2016.
  */
object MergeSort {

  type SortedList = List[Int]

  def mergeSort(list: List[Int]): SortedList = list match {
    case h :: Nil => list
    case h :: t =>
      if (t.length == 1) {
        if (h < t.head)
          list
        else
          list.reverse
      } else {
        val split = list.splitAt(list.length / 2)
        mergeAndSort(mergeSort(split._1), mergeSort(split._2))
      }
    case _ => list
  }

  def mergeAndSort(a: SortedList, b: SortedList): SortedList = {
    val result = ListBuffer[Int]()
    var i, j = 0
    while (j < b.length || i < a.length) {
      if (j >= b.length) {
        //b's end reached
        result += a(i)
        i += 1
      } else if (i >= a.length) {
        //a's end reached
        result += b(j)
        j += 1
      } else {
        if (a(i) < b(j)) {
          result += a(i)
          i += 1
        } else {
          result += b(j)
          j += 1
        }
      }
    }
    result.toList
  }

}
