package week1

import benchmark.Performance.timeIt
import week1.CountInversions._

import scala.io.Source

object MergeSortAssignment {

  def main(args: Array[String]): Unit = {
    val lines = Source.fromFile("src\\resources\\week1_IntegerArray.txt").getLines.toList.map(_.toInt)
    val result = timeIt(countInversions(lines).inversions)
    println(s"Number of inversions to sort with mergeSort: ${result._1}\n${result._2}")
  }

}