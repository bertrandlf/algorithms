package week2

/**
  * Created by Bertrand on 14/09/2016.
  * Taken from Scala by example by M. Odersky
  * http://www.scala-lang.org/docu/files/ScalaByExample.pdf
  */
object FunctionalQuickSort {

  def quickSort(xs: Array[Int]): Array[Int] = {
    if (xs.length <= 1) xs
    else {
      val pivot = xs(xs.length / 2)
      Array.concat(
        quickSort(xs filter (pivot >)),
        xs filter (pivot ==),
        quickSort(xs filter (pivot <)))
     }
  }

}
