package week3

import scala.collection.immutable.HashMap

/**
  * Created by Bertrand on 25/09/2016.
  */
package object model {
  type Edge = (Vertex, Vertex)
  type Edges = List[Edge]
  type Vertices = HashMap[String, Vertex]
}
