package week4

import benchmark.Performance.timeIt
import week4.model._

import scala.collection.mutable

/**
  * Created by Bertrand on 29/09/2016.
  */
object Kosaraju {

  def main(args: Array[String]): Unit = {
    val result = timeIt(fiveSccFromFile("src/resources/week4/week4_SCC.txt").mkString(","))
    println(result._2 + "\n" + result._1)
  }

  def fiveSccFromFile(fileName: String): List[Int] = {
    //each node now has a leader
    val leaders = getLeaders(DirectedGraph.fromFile(fileName))
    //count how many times each leader appears
    val leaderSize = leaders.zipWithIndex.groupBy(_._1).mapValues(_.length)
    //sort by length, decreasing
    leaderSize.toSeq.sortWith(_._2 > _._2).take(5).map(_._2).toList
  }

  /** For each node in the given order
    *- add it to the stack (start the stack, it will always be empty for this step)
    *- fill stack by going down all the way
    *- once we are at the bottom (node with no neighbours)
    *- pop the stack and apply given function for start node and popped node
    * **/
  private def dfsLoop(graph: DirectedGraph, order: Iterable[Int])(f: (Int, Int) => Unit) {
    val visited = Array.fill(graph.size)(false)
    val stack = mutable.Stack[Int]()
    for (node <- order) {
      if (!visited(node)) {
        stack.push(node)
        while (stack.nonEmpty) {
          val visiting = stack.head
          visited(visiting) = true
          val neighbours = graph.adj(visiting).filterNot(visited)
          if (neighbours.nonEmpty)
            stack.push(neighbours.head)
          else {
            stack.pop()
            f(visiting, node)
          }
        }
      }
    }
  }

  private def getLeaders(graph: DirectedGraph): List[Int] = {
    val size = graph.size
    val weights = Array.fill(size)(0)
    val leaders = Array.fill(size)(0)
    var t = 0
    dfsLoop(graph.reversed, 0 until size) { (node, _) =>
      weights(node) = t
      t += 1
    }
    dfsLoop(graph, (1 until size).sortBy(-weights(_))) {
      (node, visiting) => {
        leaders(node) = visiting
      }
    }
    leaders.toList
  }
}