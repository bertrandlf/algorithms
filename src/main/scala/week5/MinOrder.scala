package week5

/**
  * Created by Bertrand on 7/10/2016.
  */
object MinOrder extends Ordering[NodePath] {
  override def compare(x: NodePath, y: NodePath): Int = y._2 compare x._2
}
