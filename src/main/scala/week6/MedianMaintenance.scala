package week6

import benchmark.Performance

import scala.io.{BufferedSource, Source}

/**
  * Created by Bertrand on 14/10/2016.
  */
object MedianMaintenance {

  def main(args: Array[String]) = {
    val input = Source.fromFile("src/resources/week6/week6-median.txt")
    val result = Performance.timeIt(medianMod(input).getMedianSum % 10000)
    println(s"Median maintenance ${result._1}")
    println(s"result = ${result._2}")
  }

  def medianMod(input: BufferedSource): MedianHeap = {
    val medianHeap = new MedianHeap()
    input.getLines().foreach({ line =>
      medianHeap.enqueue(line.toInt)
    })
    medianHeap
  }
}
