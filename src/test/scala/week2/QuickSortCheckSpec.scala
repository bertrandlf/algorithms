package week2

import org.scalacheck.Prop._
import org.scalacheck.Properties
import week2.model.QuickSortList

/**
  * Created by Bertrand on 14/09/2016.
  */
object QuickSortCheckSpec extends Properties("QuickSort") {

  property("sorts a given list with FIRST element as pivot for partition") = forAll { (a: Array[Int]) =>
    val expected = a.sorted.toList
    QuickSort.quickSort(new QuickSortList(a, 0), 0, a.length - 1, QuickSort.partitionFirst)
    a.toList == expected
  }

  property("sorts a given list with LAST element as pivot for partition") = forAll { (a: Array[Int]) =>
    val expected = a.sorted.toList
    QuickSort.quickSort(new QuickSortList(a, 0), 0, a.length - 1, QuickSort.partitionLast)
    a.toList == expected
  }

  property("sorts a given list with MEDIAN element as pivot for partition") = forAll { (a: Array[Int]) =>
    val expected = a.sorted.toList
    QuickSort.quickSort(new QuickSortList(a, 0), 0, a.length - 1, QuickSort.partitionMedian)
    a.toList == expected
  }

}
