package week3

import org.scalatest.{FlatSpec, Matchers}
import week3.model._

import scala.collection.immutable.HashMap

/**
  * Created by Bertrand on 22/09/2016.
  */
class GraphReductionSpec extends FlatSpec with Matchers {

  val v1 = new Vertex("1")
  val v2 = new Vertex("2")
  val v3 = new Vertex("3")
  val v4 = new Vertex("4")
  val vertices = HashMap(v1.value -> v1, v2.value -> v2, v3.value -> v3, v4.value -> v4)
  val edges = List((v1, v2), (v1, v3), (v1, v4), (v2, v4), (v3, v4))
  val graph = new Graph(vertices, edges)

  "A graph" should "be reduced once" in {
    GraphReduction.reduce(graph).size shouldEqual 3
  }

  "A graph" should "be reduced down two vertices (cut)" in {
    GraphReduction.reduceToCut(graph).size shouldEqual 2
  }

}
