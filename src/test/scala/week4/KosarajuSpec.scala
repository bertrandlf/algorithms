package week4

import org.scalatest.{FlatSpec, Matchers}
import week4.Kosaraju._

/**
  * Created by Bertrand on 29/09/2016.
  */
class KosarajuSpec extends FlatSpec with Matchers {
  val courseExampleFilePath: String = "src/resources/week4/week4_course_example.txt"

  "Finding SCC sizes for the course's example" should "yield the expected list" in {
    fiveSccFromFile(courseExampleFilePath) shouldEqual List(3, 3, 3)
  }

  "Finding SCC sizes for test graph 7 1" should "yield the expected list" in {
    fiveSccFromFile("src/resources/week4/week4_test_graph_7_1.txt") shouldEqual List(7, 1)
  }

  "Finding SCC sizes for test graph 6 3 2 1" should "yield the expected list" in {
    fiveSccFromFile("src/resources/week4/week4_test_graph_6_3_2_1.txt") shouldEqual List(6, 3, 2, 1)
  }
}