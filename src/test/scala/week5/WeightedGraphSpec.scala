package week5

import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by Bertrand on 19/10/2016.
  */
class WeightedGraphSpec extends FlatSpec with Matchers {

  val testFileSmall: String = "src/resources/week5/week5_weightedGraphSmall.txt"
  val testFileMedium: String = "src/resources/week5/week5_weightedGraphMedium.txt"

  it should "create a graph from a file" in {
    val weightedGraph = WeightedGraph.fromFile(testFileSmall)
    weightedGraph.NbEdges shouldEqual 18
  }

  "computeDistances" should "return min distances given a node of the small graph" in {
    val weightedGraph = WeightedGraph.fromFile(testFileSmall)
    weightedGraph.distances(0) shouldEqual Array(0, 3, 2, 4, 5, 5)
  }

  "computeDistances" should "return min distances given a node of the medium graph" in {
    val weightedGraph = WeightedGraph.fromFile(testFileMedium)
    weightedGraph.distances(0) shouldEqual Array(0, 1, 4, 5, 3, 4, 3, 2, 3, 6, 5)
  }
}
