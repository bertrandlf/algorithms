package week6

import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by Bertrand on 13/10/2016.
  */
class TwoSumSpec extends FlatSpec with Matchers {

  "binaryFind" should "find a given element" in {
    TwoSum.binaryFind(4, Array(1,2,3,4,5,6)) shouldEqual true
  }

  "binaryFind" should "not find a non existing given element" in {
    TwoSum.binaryFind(10, Array(1,2,3,4,5,6)) shouldEqual false
  }

  "findTwoSums" should "find expected number of sums" in {
    val data = TwoSum.fillDataStructures("src/resources/week6/week6-2sum-test.txt")
    val range = -10000 to 10000
    val hashTable = data._1
    val inputList = data._2
    TwoSum.findTwoSums(hashTable, inputList.toArray.sorted, range) shouldEqual 14
  }

  "findTwoSumsBinarySearch" should "find expected number of sums" in {
    val data = TwoSum.fillDataStructures("src/resources/week6/week6-2sum-test.txt")
    val range = -10000 to 10000
    val inputList = data._2
    TwoSum.findTwoSumsBinarySearch(inputList.toArray.sorted, range) shouldEqual 14
  }
}
